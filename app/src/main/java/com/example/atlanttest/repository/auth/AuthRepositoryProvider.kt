package com.example.atlanttest.repository.auth

object AuthRepositoryProvider {
    private lateinit var authRepository: AuthRepository

    fun provideAuthRepository(): AuthRepository {
        if (!this::authRepository.isInitialized) {
            authRepository = AuthRepositoryInit()
        }
        return authRepository
    }
}
