package com.example.atlanttest.repository.auth

import com.example.atlanttest.data.model.AuthToken
import com.example.atlanttest.data.model.UserInfo
import io.reactivex.Completable
import io.reactivex.Observable

interface AuthRepository {

    fun tryToLogin(email: String, password: String): Observable<AuthToken>
    fun refreshToken(): Observable<AuthToken>
    fun tryToLogout(sessionId: String): Completable
    fun getUserInfo(): Observable<UserInfo>
}