package com.example.atlanttest.repository.websocket

import com.example.atlanttest.screen.base.interfaces.WebSocketEvents
import com.example.atlanttest.utils.apiUtils.JsonSerializer
import com.example.atlanttest.utils.apiUtils.RxUtils
import io.reactivex.Flowable
import org.java_websocket.client.WebSocketClient
import org.java_websocket.handshake.ServerHandshake
import java.net.URI
import java.net.URISyntaxException

class WebSocketRepositoryInit : WebSocketRepository {

    private lateinit var webSocketClient: WebSocketClient

    companion object {
        private const val SOCKET_URI = "wss://ws.blockchain.info/inv"
    }

    override fun connect(webSocketEvents: WebSocketEvents) {
        val uri: URI
        try {
            uri = URI(SOCKET_URI)
        } catch (e: URISyntaxException) {
            e.printStackTrace()
            return
        }

        webSocketClient = object : WebSocketClient(uri) {
            override fun onOpen(handshakedata: ServerHandshake?) {
                webSocketEvents.onOpen(handshakedata)
            }

            override fun onClose(code: Int, reason: String?, remote: Boolean) {
                webSocketEvents.onClose(code, reason, remote)
            }

            override fun onMessage(message: String?) {
                webSocketEvents.onMessage(message)
            }

            override fun onError(ex: Exception?) {
                webSocketEvents.onError(ex)
            }
        }

        webSocketClient.connect()
    }

    override fun disconnect() {
        webSocketClient.close()
    }

    override fun subscribeToUnconfirmedTransactions(): Flowable<Unit> {
        return Flowable.just(webSocketClient.send(JsonSerializer.getParamsToSubscribeTransactions().toString()))
            .compose(RxUtils.flowableAsync())
    }

    override fun unsubscribeToUnconfirmedTransaction(): Flowable<Unit> {
        return Flowable.just(webSocketClient.send(JsonSerializer.getParamsToUnsubscribeTransactions().toString()))
            .compose(RxUtils.flowableAsync())
    }
}