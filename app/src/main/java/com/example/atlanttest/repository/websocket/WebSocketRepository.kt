package com.example.atlanttest.repository.websocket

import com.example.atlanttest.screen.base.interfaces.WebSocketEvents
import io.reactivex.Flowable

interface WebSocketRepository {
    fun connect(webSocketEvents: WebSocketEvents)
    fun disconnect()
    fun subscribeToUnconfirmedTransactions(): Flowable<Unit>
    fun unsubscribeToUnconfirmedTransaction(): Flowable<Unit>
}