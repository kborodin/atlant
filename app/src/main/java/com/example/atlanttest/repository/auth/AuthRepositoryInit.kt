package com.example.atlanttest.repository.auth

import com.example.atlanttest.data.model.AuthToken
import com.example.atlanttest.data.model.UserInfo
import com.example.atlanttest.network.ApiFactory
import com.example.atlanttest.utils.apiUtils.JsonSerializer
import com.example.atlanttest.utils.apiUtils.RxUtils
import io.reactivex.Completable
import io.reactivex.Observable

class AuthRepositoryInit : AuthRepository {

    override fun tryToLogin(email: String, password: String): Observable<AuthToken> {
        return ApiFactory.create()
            .tryToLogin(JsonSerializer.getLoginParams(email, password))
            .compose(RxUtils.async())
    }

    override fun refreshToken(): Observable<AuthToken> {
        return ApiFactory.createWithToken()
            .refreshToken()
            .compose(RxUtils.async())
    }

    override fun tryToLogout(sessionId: String): Completable {
        return ApiFactory.createWithToken()
            .tryToLogout(JsonSerializer.getLogoutParams(sessionId))
            .compose(RxUtils.completableAsync())
    }

    override fun getUserInfo(): Observable<UserInfo> {
        return ApiFactory.createWithToken()
            .getUserInfo()
            .compose(RxUtils.async())
    }
}