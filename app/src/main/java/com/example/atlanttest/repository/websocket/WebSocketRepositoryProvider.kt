package com.example.atlanttest.repository.websocket

object WebSocketRepositoryProvider {

    private lateinit var webSocketRepository: WebSocketRepository

    fun provideWebSocketRepository(): WebSocketRepository {
        if (!this::webSocketRepository.isInitialized) {
            webSocketRepository = WebSocketRepositoryInit()
        }
        return webSocketRepository
    }
}