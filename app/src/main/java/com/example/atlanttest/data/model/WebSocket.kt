package com.example.atlanttest.data.model

import androidx.annotation.Nullable
import com.google.gson.annotations.SerializedName
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class UnconfirmedTransaction(
    @SerializedName("op")
    val op: String,
    @SerializedName("x")
    val x: X
) {

    data class X(
        @SerializedName("lock_time") @Nullable
        val lockTime: Int?,
        @SerializedName("ver")
        val ver: Int,
        @SerializedName("size")
        val size: Int,
        @SerializedName("inputs")
        val inputs: List<Input>,
        @SerializedName("time")
        val time: Int,
        @SerializedName("tx_index") @Nullable
        val txIndex: Int?,
        @SerializedName("vin_sz") @Nullable
        val vinSz: Int?,
        @SerializedName("hash")
        val hash: String,
        @SerializedName("vout_sz") @Nullable
        val voutSz: Int?,
        @SerializedName("relayed_by") @Nullable
        val relayedBy: String?,
        @SerializedName("out")
        val `out`: List<Out>
    ) {

        data class Input(
            @SerializedName("sequence")
            val sequence: Long,
            @SerializedName("prev_out") @Nullable
            val prevOut: PrevOut?,
            @SerializedName("script")
            val script: String
        ) {

            data class PrevOut(
                @SerializedName("spent")
                val spent: Boolean,
                @SerializedName("tx_index")
                val txIndex: Long,
                @SerializedName("type")
                val type: Long,
                @SerializedName("addr") @Nullable
                val addr: String?,
                @SerializedName("value")
                val value: Long,
                @SerializedName("n")
                val n: Long,
                @SerializedName("script")
                val script: String
            )
        }

        data class Out(
            @SerializedName("spent")
            val spent: Boolean,
            @SerializedName("tx_index") @Nullable
            val txIndex: Int?,
            @SerializedName("type")
            val type: Int,
            @SerializedName("addr") @Nullable
            val addr: String?,
            @SerializedName("value")
            val value: Long,
            @SerializedName("n")
            val n: Int,
            @SerializedName("script")
            val script: String
        )
    }
}