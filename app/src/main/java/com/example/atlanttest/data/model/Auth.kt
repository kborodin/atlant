package com.example.atlanttest.data.model
import com.google.gson.annotations.SerializedName

data class AuthToken(
    @SerializedName("token")
    val token: String
)

data class UserInfo(
    @SerializedName("info")
    val info: Info
) {
    data class Info(
        @SerializedName("session")
        val session: Any,
        @SerializedName("account")
        val account: Account,
        @SerializedName("profiles")
        val profiles: List<Profile>
    ) {
        data class Account(
            @SerializedName("account_id")
            val accountId: String,
            @SerializedName("account_type")
            val accountType: String,
            @SerializedName("email")
            val email: String,
            @SerializedName("email_verified")
            val emailVerified: Boolean,
            @SerializedName("phone")
            val phone: String,
            @SerializedName("totp_verified")
            val totpVerified: Boolean,
            @SerializedName("2fa_method")
            val faMethod: Any,
            @SerializedName("password")
            val password: String,
            @SerializedName("created_at")
            val createdAt: String
        )

        data class Profile(
            @SerializedName("profile_id")
            val profileId: String,
            @SerializedName("account_id")
            val accountId: String,
            @SerializedName("profile_type")
            val profileType: String,
            @SerializedName("first_name")
            val firstName: String,
            @SerializedName("last_name")
            val lastName: String,
            @SerializedName("location")
            val location: String,
            @SerializedName("email")
            val email: String,
            @SerializedName("avatar_url")
            val avatarUrl: String,
            @SerializedName("kyc_verified")
            val kycVerified: Boolean,
            @SerializedName("langs_spoken_names")
            val langsSpokenNames: List<Any>,
            @SerializedName("joined_at")
            val joinedAt: String,
            @SerializedName("gender")
            val gender: Any,
            @SerializedName("phone_country")
            val phoneCountry: Any,
            @SerializedName("phone_number")
            val phoneNumber: Any
        )
    }
}