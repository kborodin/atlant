package com.example.atlanttest.utils

import android.app.Activity
import android.content.Context
import android.content.res.Resources
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.example.atlanttest.BuildConfig
import com.example.atlanttest.R

// shows this view
fun View.show(): View {
    this.visibility = View.VISIBLE
    return this
}

// makes the view gone
fun View.gone(): View {
    this.visibility = View.GONE
    return this
}

fun View.invisible(): View {
    this.visibility = View.INVISIBLE
    return this
}

fun View.enable(): View {
    this.isEnabled = true
    return this
}

fun View.disable(): View {
    this.isEnabled = false
    return this
}

fun Context.showShortToast(textRes: Int) {
    showShortToast(getString(textRes))
}

fun Context.showShortToast(text: String?) {
    Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
}

fun dpToPx(dp: Int): Int {
    return (dp * Resources.getSystem().displayMetrics.density).toInt()
}

fun isEmailValid(email: CharSequence): Boolean {
    return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
}

fun Activity.logd(message: String) {
    if (BuildConfig.DEBUG) Log.d(this::class.java.simpleName, message)
}

fun Fragment.logd(message: String) {
    if (BuildConfig.DEBUG) Log.d(this::class.java.simpleName, message)
}

fun Context.showExitAlertDialog(
    positiveButton: (AlertDialog.Builder) -> Unit
) {
    val dialog = AlertDialog.Builder(this)
    dialog.setMessage(getString(R.string.are_you_sure_you_want_to_exit))
        .setCancelable(false)
        .setPositiveButton(getString(android.R.string.yes)) { _, _ -> positiveButton(dialog) }
        .setNegativeButton(getString(android.R.string.no), null)
        .show()
}

fun Context.showSimpleAlertDialog(
    title: String,
    positiveButton: (AlertDialog.Builder) -> Unit
) {
    val dialog = AlertDialog.Builder(this)
    dialog.setMessage(title)
        .setCancelable(false)
        .setPositiveButton(getString(android.R.string.yes)) { _, _ -> positiveButton(dialog) }
        .setNegativeButton(getString(android.R.string.no), null)
        .show()
}
