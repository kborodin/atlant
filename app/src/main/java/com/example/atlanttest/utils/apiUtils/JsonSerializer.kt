package com.example.atlanttest.utils.apiUtils

import com.google.gson.JsonObject

object JsonSerializer {

    private const val EMAIL = "email"
    private const val PASSWORD = "password"
    private const val SESSION_ID = "session_id"
    private const val OP = "op"
    private const val UNCONFIRMED_SUB = "unconfirmed_sub"
    private const val UNCONFIRMED_UNSUB = "unconfirmed_unsub"

    fun getLoginParams(email: String, password: String): JsonObject {
        val param = JsonObject()
        param.addProperty(EMAIL, email)
        param.addProperty(PASSWORD, password)
        return param
    }

    fun getLogoutParams(sessionId: String): JsonObject {
        val param = JsonObject()
        param.addProperty(SESSION_ID, sessionId)
        return param
    }

    fun getParamsToSubscribeTransactions(): JsonObject {
        val param = JsonObject()
        param.addProperty(OP, UNCONFIRMED_SUB)
        return param
    }

    fun getParamsToUnsubscribeTransactions(): JsonObject {
        val param = JsonObject()
        param.addProperty(OP, UNCONFIRMED_UNSUB)
        return param
    }
}