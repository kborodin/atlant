package com.example.atlanttest.utils.apiUtils

const val AUTH = "auth"
const val SESSIONS = "sessions"
const val REFRESH = "/refresh"
const val END = "/end"
const val CURRENT = "current"