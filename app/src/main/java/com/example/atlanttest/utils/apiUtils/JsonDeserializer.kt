package com.example.atlanttest.utils.apiUtils

import com.example.atlanttest.data.model.UnconfirmedTransaction
import com.squareup.moshi.Moshi

object JsonDeserializer {

    fun getUnconfirmedTransaction(moshi: Moshi, message: String): UnconfirmedTransaction? {
        val jsonAdapter = moshi.adapter(UnconfirmedTransaction::class.java)
        return jsonAdapter.fromJson(message)
    }
}