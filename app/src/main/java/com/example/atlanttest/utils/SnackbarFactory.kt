package com.example.atlanttest.utils

import android.content.Context
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import com.example.atlanttest.R
import com.google.android.material.snackbar.Snackbar

object SnackbarFactory {

    fun showNetworkError(context: Context, v: View): Snackbar {
        val snackbar = Snackbar.make(
            v, spannableColoredText(
                context, android.R.color.white,
                context.getString(R.string.can_t_connect_to_server_check_internet_connection)
            ), Snackbar.LENGTH_SHORT
        )
        snackbar.config(context)
        snackbar.view.setOnClickListener { snackbar.dismiss() }
        return snackbar
    }

    fun showServerUnavailableError(context: Context, v: View): Snackbar {
        val snackbar = Snackbar.make(
            v, spannableColoredText(
                context, android.R.color.white,
                context.resources.getString(R.string.server_is_unavailable_error_message)
            ),
            Snackbar.LENGTH_SHORT
        )
        snackbar.config(context)
        snackbar.view.setOnClickListener { snackbar.dismiss() }
        return snackbar
    }

    fun showUnexpectedError(context: Context, v: View): Snackbar {
        val snackbar = Snackbar.make(
            v, spannableColoredText(
                context, android.R.color.white,
                context.getString(R.string.something_failed_try_again)
            ),
            Snackbar.LENGTH_SHORT
        )
        snackbar.config(context)
        snackbar.view.setOnClickListener { snackbar.dismiss() }
        return snackbar
    }

    fun showTimeoutError(context: Context, v: View): Snackbar {
        val snackbar = Snackbar.make(
            v, spannableColoredText(
                context, android.R.color.white,
                context.getString(R.string.server_is_not_responding_try_later)
            ),
            Snackbar.LENGTH_SHORT
        )
        snackbar.config(context)
        snackbar.view.setOnClickListener { snackbar.dismiss() }
        return snackbar
    }

    fun showNewError(context: Context, v: View, text: String): Snackbar {
        val snackbar =
            Snackbar.make(v, spannableColoredText(context, android.R.color.white, text), Snackbar.LENGTH_LONG)
        snackbar.view.setOnClickListener { snackbar.dismiss() }
        snackbar.config(context)
        return snackbar
    }




    private fun Snackbar.config(context: Context) {
        val params = this.view.layoutParams as ViewGroup.MarginLayoutParams
        params.setMargins(dpToPx(15), 0, dpToPx(15), dpToPx(10))
        this.view.layoutParams = params
        this.view.background = ContextCompat.getDrawable(context, R.drawable.snackbar_shape)
        this.view.findViewById<TextView>(R.id.snackbar_text).textSize = 15f
        ViewCompat.setElevation(this.view, 6f)
        this.show()
    }

    private fun spannableColoredText(
        context: Context, textColorResID: Int,
        text: CharSequence
    ): SpannableStringBuilder {
        val whiteSpan = ForegroundColorSpan(ContextCompat.getColor(context, textColorResID))
        val snackbarText = SpannableStringBuilder(text)
        snackbarText.setSpan(whiteSpan, 0, snackbarText.length, Spanned.SPAN_INCLUSIVE_INCLUSIVE)
        return snackbarText
    }

}