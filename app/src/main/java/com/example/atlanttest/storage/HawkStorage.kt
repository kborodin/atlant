package com.example.atlanttest.storage

import com.orhanobut.hawk.Hawk

private const val JWT = "json_web_token"

fun saveAuthData(jwt: String) {
    Hawk.put(JWT, jwt)
}

fun getAuthData(): String? {
    if (Hawk.contains(JWT)) {
        return Hawk.get(JWT)
    }
    return null
}