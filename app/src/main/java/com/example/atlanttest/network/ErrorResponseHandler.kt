package com.example.atlanttest.network

import com.example.atlanttest.screen.base.interfaces.BaseView
import com.example.atlanttest.utils.SnackbarFactory
import retrofit2.HttpException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import javax.net.ssl.HttpsURLConnection

object ErrorResponseHandler {

    fun handleError(
        error: Throwable,
        view: BaseView.View
    ) {
        when (error) {
            is HttpException ->
                when (error.code()) {
                    HttpsURLConnection.HTTP_BAD_GATEWAY -> {
                        SnackbarFactory.showServerUnavailableError(view.context(), view.rootView())
                    }
                    HttpsURLConnection.HTTP_UNAVAILABLE -> {
                        SnackbarFactory.showServerUnavailableError(view.context(), view.rootView())
                    }

                    else -> {
                        view.showUnexpectedError(error)
                        error.printStackTrace()
                    }
                }
            is UnknownHostException -> SnackbarFactory.showNetworkError(view.context(), view.rootView())
            is SocketTimeoutException -> SnackbarFactory.showTimeoutError(view.context(), view.rootView())
            else -> {
                view.showUnexpectedError(error)
                error.printStackTrace()
            }
        }
    }
}


