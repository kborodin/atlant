package com.example.atlanttest.network

import com.example.atlanttest.BuildConfig
import com.example.atlanttest.service.ApiService
import com.example.atlanttest.storage.getAuthData
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ApiFactory {
    private const val BASE_API_URL = "https://api.dev.karta.com/accounts/"

    fun create(): ApiService {
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_API_URL)
            .client(buildClient())
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()

        return retrofit.create(ApiService::class.java)
    }

    private fun buildClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .readTimeout(10, TimeUnit.SECONDS)
            .connectTimeout(10, TimeUnit.SECONDS)
            .addInterceptor(HttpLoggingInterceptor().apply {
                level = if (BuildConfig.DEBUG) Level.BODY else Level.NONE
            })
            .addInterceptor { chain ->
                chain.proceed(chain.request())
            }
            .build()
    }

    fun createWithToken(): ApiService {
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_API_URL)
            .client(buildRegisteredClient())
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()

        return retrofit.create(ApiService::class.java)
    }

    private fun buildRegisteredClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .readTimeout(10, TimeUnit.SECONDS)
            .connectTimeout(10, TimeUnit.SECONDS)
            .addInterceptor(HttpLoggingInterceptor().apply {
                level = if (BuildConfig.DEBUG) Level.BODY else Level.NONE
            })
            .addInterceptor { chain ->
                val jwt = getAuthData()
                if (jwt != null) {
                    val request = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer $jwt")
                        .build()
                    chain.proceed(request)
                } else {
                    chain.proceed(chain.request())
                }
            }
            .build()
    }
}