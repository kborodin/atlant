package com.example.atlanttest.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.atlanttest.R
import com.example.atlanttest.adapter.TransactionAdapter.ViewHolder
import com.example.atlanttest.data.model.UnconfirmedTransaction
import kotlinx.android.synthetic.main.item_transaction.view.tvDate
import kotlinx.android.synthetic.main.item_transaction.view.tvTransactionHash
import java.util.Date

class TransactionAdapter(private val transactionList: ArrayList<UnconfirmedTransaction>) :
    RecyclerView.Adapter<ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_transaction, parent, false))
    }

    override fun getItemCount(): Int = transactionList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val transaction = transactionList[position]
        val timeOfArrival = Date(transaction.x.time * 1000L)
        holder.tvDate.text = timeOfArrival.toString()
        holder.tvTransactionHash.text = transaction.x.hash
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvDate = itemView.tvDate!!
        val tvTransactionHash = itemView.tvTransactionHash!!
    }
}