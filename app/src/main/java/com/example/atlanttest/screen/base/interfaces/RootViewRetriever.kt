package com.example.atlanttest.screen.base.interfaces

import androidx.coordinatorlayout.widget.CoordinatorLayout

interface RootViewRetriever {
    fun rootView(): CoordinatorLayout
}