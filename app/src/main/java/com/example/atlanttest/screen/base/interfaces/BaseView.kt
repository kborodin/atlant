package com.example.atlanttest.screen.base.interfaces

interface BaseView<T : BasePresenter> {

    interface View : ContextRetriever, ErrorsView, LoadingView, RootViewRetriever

    fun setPresenter(presenter: T)
}
