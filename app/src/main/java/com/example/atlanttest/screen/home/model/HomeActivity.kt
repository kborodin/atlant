package com.example.atlanttest.screen.home.model

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.View.OnClickListener
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.atlanttest.R
import com.example.atlanttest.R.string
import com.example.atlanttest.adapter.TransactionAdapter
import com.example.atlanttest.data.model.UnconfirmedTransaction
import com.example.atlanttest.data.model.UserInfo
import com.example.atlanttest.screen.auth.model.LoginActivity.Companion.startLoginActivity
import com.example.atlanttest.screen.home.presenter.HomePresenter
import com.example.atlanttest.screen.home.view.HomeView
import com.example.atlanttest.screen.home.view.HomeView.Presenter
import com.example.atlanttest.utils.SnackbarFactory
import com.example.atlanttest.utils.disable
import com.example.atlanttest.utils.enable
import com.example.atlanttest.utils.gone
import com.example.atlanttest.utils.logd
import com.example.atlanttest.utils.show
import com.example.atlanttest.utils.showExitAlertDialog
import com.example.atlanttest.utils.showSimpleAlertDialog
import com.orhanobut.hawk.Hawk
import kotlinx.android.synthetic.main.activity_home.btnLogout
import kotlinx.android.synthetic.main.activity_home.btnRefreshConnection
import kotlinx.android.synthetic.main.activity_home.btnReset
import kotlinx.android.synthetic.main.activity_home.btnStart
import kotlinx.android.synthetic.main.activity_home.btnStop
import kotlinx.android.synthetic.main.activity_home.ivIndicator
import kotlinx.android.synthetic.main.activity_home.llTryToReconnect
import kotlinx.android.synthetic.main.activity_home.pbHome
import kotlinx.android.synthetic.main.activity_home.recyclerTransactions
import kotlinx.android.synthetic.main.activity_home.rootView
import kotlinx.android.synthetic.main.activity_home.tvConnectionStatus
import kotlinx.android.synthetic.main.activity_home.tvLogout
import kotlinx.android.synthetic.main.activity_home.tvTotalTransactionsSum
import kotlinx.android.synthetic.main.activity_home.tvUserName

class HomeActivity : AppCompatActivity(), HomeView.View, OnClickListener {

    private lateinit var presenter: Presenter
    private lateinit var transactionsAdapter: TransactionAdapter
    private lateinit var transactionList: ArrayList<UnconfirmedTransaction>
    private lateinit var rotationAnimation: Animation

    companion object {
        fun Activity.startHomeActivity() {
            startActivity(Intent(this, HomeActivity::class.java))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setPresenter(HomePresenter(this))
        presenter.subscribe()
        presenter.tryToConnectToWebSocketServer()
        initLogoutButton()
        initRecycler()
        btnStart.setOnClickListener(this)
        btnStop.setOnClickListener(this)
        btnReset.setOnClickListener(this)
        btnRefreshConnection.setOnClickListener(this)
        setTotalTransactionsValue()
        setConnectionStatusViews(false)
    }

    private fun initLogoutButton() {
        btnLogout.setOnClickListener {
            showSimpleAlertDialog(getString(R.string.are_you_sure_you_want_to_sign_out)) {
                presenter.tryToLogout()
            }
        }
    }

    private fun initRecycler() {
        transactionList = ArrayList()
        transactionsAdapter = TransactionAdapter(transactionList)
        val layoutManager = LinearLayoutManager(this)
        layoutManager.reverseLayout = true
        layoutManager.stackFromEnd = true
        recyclerTransactions.layoutManager = layoutManager
        val itemDecorator = DividerItemDecoration(this, layoutManager.orientation)
        recyclerTransactions.addItemDecoration(itemDecorator)
        recyclerTransactions.adapter = transactionsAdapter
    }

    override fun onClick(view: View?) {
        when (view) {
            btnStart -> presenter.subscribeToUnconfirmedTransactions()
            btnStop -> {
                presenter.unsubscribeToUnconfirmedTransaction()
            }
            btnReset -> {
                transactionList.clear()
                transactionsAdapter.notifyDataSetChanged()
                setTotalTransactionsValue()
            }
            btnRefreshConnection -> {
                presenter.tryToConnectToWebSocketServer()
                startRefreshAnimation()
            }
        }
    }

    private fun startRefreshAnimation() {
        rotationAnimation = AnimationUtils.loadAnimation(this, R.anim.rotation)
        btnRefreshConnection.startAnimation(rotationAnimation)
    }

    private fun stopRefreshAnimation() {
        if (this::rotationAnimation.isInitialized) {
            if (rotationAnimation.isInitialized) {
                rotationAnimation.cancel()
            }
        }
    }

    private fun setTotalTransactionsValue() {
        tvTotalTransactionsSum.text =
            String.format("${getString(R.string.total_transactions)} ${transactionList.size}")
    }

    private fun setConnectionStatusViews(isConnected: Boolean) {
        if (isConnected) {
            ivIndicator.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_indicator_green))
            tvConnectionStatus.text = getString(string.connected)
            llTryToReconnect.gone()
        } else {
            ivIndicator.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_indicator_red))
            tvConnectionStatus.text = getString(string.disconnected)
            llTryToReconnect.show()
        }
    }

    //HomeView.View
    override fun notifyUserInfoRetrieved(userInfo: UserInfo) {
        val profiles = userInfo.info.profiles
        if (profiles.isNullOrEmpty().not()) {
            tvUserName.text =
                String.format(
                    "${getString(R.string.welcome_back)}," +
                            " ${userInfo.info.profiles[0].firstName} ${userInfo.info.profiles[0].lastName}"
                )
        }
    }

    override fun notifyUserLoggedOut() {
        if (Hawk.deleteAll()) {
            startLoginActivity()
            finishAffinity()
            overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right)
        }
    }

    override fun setPresenter(presenter: Presenter) {
        this.presenter = presenter
    }

    override fun context(): Context = this

    override fun showUnexpectedError(error: Throwable) {
        val errorMessage = error.message
        if (errorMessage != null) {
            logd(errorMessage)
            SnackbarFactory.showNewError(this, rootView(), errorMessage)
            presenter.unsubscribeToUnconfirmedTransaction()
        }
    }

    override fun showLoading() {
        tvLogout.gone()
        pbHome.show()
    }

    override fun hideLoading() {
        pbHome.gone()
        tvLogout.show()
    }

    override fun rootView(): CoordinatorLayout = rootView

    override fun notifySocketConnectionOpened() {
        runOnUiThread {
            if (this::rotationAnimation.isInitialized) {
                rotationAnimation.cancel()
            }
            stopRefreshAnimation()
            setConnectionStatusViews(true)
            btnStart.enable()
            btnReset.enable()
        }
    }

    override fun notifyUnconfirmedTransactionSubscribed() {
        btnStart.disable()
        btnStop.enable()
    }

    override fun notifyUnconfirmedTransactionUnsubscribed() {
        btnStart.enable()
        btnStop.disable()
    }

    override fun notifyTransactionRetrieved(unconfirmedTransaction: UnconfirmedTransaction) {
        runOnUiThread {
            transactionList.add(unconfirmedTransaction)
            setTotalTransactionsValue()
            transactionsAdapter.notifyItemInserted(transactionList.indexOf(unconfirmedTransaction))
            recyclerTransactions.smoothScrollToPosition(transactionList.indexOf(unconfirmedTransaction))
        }
    }
    //HomeView.View

    override fun onDestroy() {
        super.onDestroy()
        presenter.unsubscribe()
    }

    override fun onBackPressed() {
        if (isTaskRoot) {
            showExitAlertDialog {
                finishAffinity()
            }
        } else {
            super.onBackPressed()
        }
    }
}
