package com.example.atlanttest.screen.base.interfaces

interface ErrorsView {
    fun showUnexpectedError(error: Throwable)
}