package com.example.atlanttest.screen.base.interfaces

import org.java_websocket.handshake.ServerHandshake

interface WebSocketEvents {
    fun onOpen(handShakeData: ServerHandshake?)

    fun onClose(code: Int, reason: String?, remote: Boolean)

    fun onMessage(message: String?)

    fun onError(ex: Exception?)
}