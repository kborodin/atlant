package com.example.atlanttest.screen.auth.presenter

import com.example.atlanttest.network.ErrorResponseHandler
import com.example.atlanttest.repository.auth.AuthRepositoryProvider
import com.example.atlanttest.screen.auth.view.LoginView
import com.example.atlanttest.storage.saveAuthData
import io.reactivex.disposables.CompositeDisposable

class LoginPresenter(private val view: LoginView.View) : LoginView.Presenter {

    private val disposable: CompositeDisposable by lazy {
        CompositeDisposable()
    }

    override fun subscribe() {}

    override fun tryToLogin(email: String, password: String) {
        disposable.add(AuthRepositoryProvider.provideAuthRepository()
            .tryToLogin(email, password)
            .doOnSubscribe { view.showLoading() }
            .doOnTerminate { view.hideLoading() }
            .subscribe(
                {
                    saveAuthData(it.token)
                    view.showHomePage()
                },

                {
                    ErrorResponseHandler.handleError(it, view)
                })
        )
    }

    override fun unsubscribe() {
        if (disposable.isDisposed) {
            disposable.clear()
        }
    }
}