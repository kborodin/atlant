package com.example.atlanttest.screen.auth.view

import com.example.atlanttest.screen.base.interfaces.BasePresenter
import com.example.atlanttest.screen.base.interfaces.BaseView

interface LoginView {
    interface View : BaseView<Presenter>, BaseView.View {
        fun showHomePage()
    }

    interface Presenter : BasePresenter {
        fun tryToLogin(email: String, password: String)
    }
}