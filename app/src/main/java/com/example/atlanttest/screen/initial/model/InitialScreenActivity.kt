package com.example.atlanttest.screen.initial.model

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.example.atlanttest.R
import com.example.atlanttest.screen.auth.model.LoginActivity.Companion.startLoginActivity
import com.example.atlanttest.screen.home.model.HomeActivity.Companion.startHomeActivity
import com.example.atlanttest.screen.initial.presenter.InitialPresenter
import com.example.atlanttest.screen.initial.view.InitialView
import com.example.atlanttest.screen.initial.view.InitialView.Presenter
import com.example.atlanttest.utils.SnackbarFactory
import com.example.atlanttest.utils.gone
import com.example.atlanttest.utils.show
import kotlinx.android.synthetic.main.activity_initial_screen.pbInitial
import kotlinx.android.synthetic.main.activity_initial_screen.rootView

class InitialScreenActivity : AppCompatActivity(), InitialView.View {

    private lateinit var presenter: Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_initial_screen)
        setPresenter(InitialPresenter(this))
        presenter.subscribe()
    }

    override fun showLoginPage() {
        startLoginActivity()
        finish()
    }

    override fun showHomePage() {
        startHomeActivity()
        finish()
    }

    override fun setPresenter(presenter: Presenter) {
        this.presenter = presenter
    }

    override fun context(): Context = this

    override fun showUnexpectedError(error: Throwable) {
        val errorMessage = error.message
        if (errorMessage != null) {
            SnackbarFactory.showNewError(this, rootView(), errorMessage)
        }
    }

    override fun showLoading() {
        pbInitial.show()
    }

    override fun hideLoading() {
        pbInitial.gone()
    }

    override fun rootView(): CoordinatorLayout = rootView

    override fun onDestroy() {
        super.onDestroy()
        presenter.unsubscribe()
    }
}
