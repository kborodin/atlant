package com.example.atlanttest.screen.base.interfaces

interface LoadingView {
    fun showLoading()
    fun hideLoading()
}