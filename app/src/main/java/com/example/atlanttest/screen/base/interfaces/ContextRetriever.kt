package com.example.atlanttest.screen.base.interfaces

import android.content.Context

interface ContextRetriever {
    fun context(): Context
}
