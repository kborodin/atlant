package com.example.atlanttest.screen.home.view

import com.example.atlanttest.data.model.UnconfirmedTransaction
import com.example.atlanttest.data.model.UserInfo
import com.example.atlanttest.screen.base.interfaces.BasePresenter
import com.example.atlanttest.screen.base.interfaces.BaseView
import com.example.atlanttest.screen.base.interfaces.WebSocketEvents

interface HomeView {
    interface View : BaseView<Presenter>, BaseView.View {

        fun notifyUserInfoRetrieved(userInfo: UserInfo)
        fun notifyUserLoggedOut()
        fun notifySocketConnectionOpened()
        fun notifyUnconfirmedTransactionSubscribed()
        fun notifyUnconfirmedTransactionUnsubscribed()
        fun notifyTransactionRetrieved(unconfirmedTransaction: UnconfirmedTransaction)
    }

    interface Presenter : BasePresenter, WebSocketEvents {
        fun tryToLogout()
        fun tryToConnectToWebSocketServer()
        fun subscribeToUnconfirmedTransactions()
        fun unsubscribeToUnconfirmedTransaction()
    }
}