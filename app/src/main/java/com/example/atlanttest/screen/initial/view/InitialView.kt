package com.example.atlanttest.screen.initial.view

import com.example.atlanttest.screen.base.interfaces.BasePresenter
import com.example.atlanttest.screen.base.interfaces.BaseView

interface InitialView {

    interface View : BaseView<Presenter>, BaseView.View {
        fun showLoginPage()
        fun showHomePage()
    }

    interface Presenter : BasePresenter
}