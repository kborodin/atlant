package com.example.atlanttest.screen.initial.presenter

import android.util.Log
import com.auth0.android.jwt.JWT
import com.example.atlanttest.network.ErrorResponseHandler
import com.example.atlanttest.repository.auth.AuthRepositoryProvider
import com.example.atlanttest.screen.initial.view.InitialView
import com.example.atlanttest.storage.getAuthData
import com.example.atlanttest.storage.saveAuthData
import io.reactivex.disposables.CompositeDisposable

class InitialPresenter(private val view: InitialView.View) : InitialView.Presenter {

    companion object {
        private const val TAG = "InitialPresenter"
    }

    private val disposable: CompositeDisposable by lazy {
        CompositeDisposable()
    }

    override fun subscribe() {
        val token = getAuthData()
        if (token != null) {
            val jwt = JWT(token)
            Log.d(TAG, "jwt expired at: ${jwt.expiresAt}")
            if (jwt.isExpired(30).not()) {
                refreshToken()
            } else {
                view.showLoginPage()
            }
        } else {
            view.showLoginPage()
        }
    }

    private fun refreshToken() {
        disposable.add(AuthRepositoryProvider.provideAuthRepository()
            .refreshToken()
            .doOnSubscribe { view.showLoading() }
            .doOnTerminate { view.hideLoading() }
            .subscribe({
                saveAuthData(it.token)
                view.showHomePage()
            }, {
                ErrorResponseHandler.handleError(it, view)
                view.showLoginPage()
            })
        )
    }

    override fun unsubscribe() {
        if (disposable.isDisposed) {
            disposable.clear()
        }
    }
}