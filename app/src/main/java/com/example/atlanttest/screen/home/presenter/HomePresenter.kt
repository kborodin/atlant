package com.example.atlanttest.screen.home.presenter

import com.auth0.android.jwt.JWT
import com.example.atlanttest.R.string
import com.example.atlanttest.network.ErrorResponseHandler
import com.example.atlanttest.repository.auth.AuthRepositoryProvider
import com.example.atlanttest.repository.websocket.WebSocketRepositoryProvider
import com.example.atlanttest.screen.home.view.HomeView
import com.example.atlanttest.storage.getAuthData
import com.example.atlanttest.utils.apiUtils.JsonDeserializer
import com.example.atlanttest.utils.apiUtils.RxUtils
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import io.reactivex.Flowable
import io.reactivex.disposables.CompositeDisposable
import org.java_websocket.handshake.ServerHandshake

class HomePresenter(private val view: HomeView.View) : HomeView.Presenter {

    companion object {
        private const val SESSION_ID = "session_id"
    }

    private val disposable: CompositeDisposable by lazy {
        CompositeDisposable()
    }

    private val moshi: Moshi by lazy {
        Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()
    }

    private fun openWebSockedConnection() {
        WebSocketRepositoryProvider.provideWebSocketRepository()
            .connect(this)
    }

    private fun closeWebSockedConnection() {
        WebSocketRepositoryProvider.provideWebSocketRepository()
            .disconnect()
    }

    override fun subscribe() {
        disposable.add(AuthRepositoryProvider.provideAuthRepository()
            .getUserInfo()
            .subscribe(view::notifyUserInfoRetrieved) {
                ErrorResponseHandler.handleError(it, view)
            }

        )
    }

    override fun tryToConnectToWebSocketServer() {
        openWebSockedConnection()
    }

    override fun tryToLogout() {
        val jwtString = getAuthData()
        if (jwtString != null) {
            val jwt = JWT(jwtString)
            val sessionId = jwt.claims[SESSION_ID].toString()
            disposable.add(AuthRepositoryProvider.provideAuthRepository()
                .tryToLogout(sessionId)
                .doOnSubscribe { view.showLoading() }
                .doOnTerminate { view.hideLoading() }
                .subscribe({
                    view.notifyUserLoggedOut()
                }, {
                    ErrorResponseHandler.handleError(it, view)
                })
            )
        } else {
            view.showUnexpectedError(Throwable(view.context().getString(string.can_t_find_token)))
        }
    }

    override fun subscribeToUnconfirmedTransactions() {
        disposable.add(
            WebSocketRepositoryProvider.provideWebSocketRepository()
                .subscribeToUnconfirmedTransactions()
                .subscribe({
                    view.notifyUnconfirmedTransactionSubscribed()
                }, {
                    view.showUnexpectedError(it)
                })
        )
    }

    override fun unsubscribeToUnconfirmedTransaction() {
        disposable.add(
            WebSocketRepositoryProvider.provideWebSocketRepository()
                .unsubscribeToUnconfirmedTransaction()
                .subscribe({
                    view.notifyUnconfirmedTransactionUnsubscribed()
                }, {
                    view.showUnexpectedError(it)
                })
        )
    }

    override fun onOpen(handShakeData: ServerHandshake?) {
        view.notifySocketConnectionOpened()
    }

    override fun onClose(code: Int, reason: String?, remote: Boolean) {
    }

    override fun onMessage(message: String?) {
        if (message != null) {
            Flowable.just(JsonDeserializer.getUnconfirmedTransaction(moshi, message))
                .compose(RxUtils.flowableAsync())
                .blockingForEach {
                    if (it != null) {
                        view.notifyTransactionRetrieved(it)
                    }
                }
        } else {
            view.showUnexpectedError(Throwable("Message is null"))
        }
    }

    override fun onError(ex: Exception?) {
        view.showUnexpectedError(Throwable(ex))
    }

    override fun unsubscribe() {
        closeWebSockedConnection()
        if (disposable.isDisposed) {
            disposable.clear()
        }
    }
}