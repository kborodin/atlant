package com.example.atlanttest.screen.base.interfaces

interface BasePresenter {

    fun subscribe()

    fun unsubscribe()
}