package com.example.atlanttest.screen.auth.model

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.example.atlanttest.R
import com.example.atlanttest.screen.auth.presenter.LoginPresenter
import com.example.atlanttest.screen.auth.view.LoginView
import com.example.atlanttest.screen.auth.view.LoginView.Presenter
import com.example.atlanttest.screen.home.model.HomeActivity.Companion.startHomeActivity
import com.example.atlanttest.utils.SnackbarFactory
import com.example.atlanttest.utils.gone
import com.example.atlanttest.utils.isEmailValid
import com.example.atlanttest.utils.show
import com.example.atlanttest.utils.showExitAlertDialog
import kotlinx.android.synthetic.main.activity_login.btnSignIn
import kotlinx.android.synthetic.main.activity_login.etEmail
import kotlinx.android.synthetic.main.activity_login.etPassword
import kotlinx.android.synthetic.main.activity_login.pbLogin
import kotlinx.android.synthetic.main.activity_login.rootView
import kotlinx.android.synthetic.main.activity_login.tvLogin

class LoginActivity : AppCompatActivity(), LoginView.View {

    private lateinit var presenter: Presenter

    companion object {
        fun Activity.startLoginActivity() {
            startActivity(Intent(this, LoginActivity::class.java))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        setPresenter(LoginPresenter(this))

        btnSignIn.setOnClickListener {
            if (checkCredentials(etEmail.text.toString(), etPassword.text.toString())) {
                presenter.tryToLogin(etEmail.text.toString(), etPassword.text.toString())
            }
        }
    }

    private fun checkCredentials(email: String, password: String): Boolean {
        hideKeyboard()
        if (email.isEmpty() && password.isEmpty()) {
            SnackbarFactory.showNewError(this, rootView(), getString(R.string.email_and_password_empty))
            return false
        } else if (email.isEmpty()) {
            SnackbarFactory.showNewError(this, rootView(), getString(R.string.email_address_is_empty))
            return false
        } else if (password.isEmpty()) {
            SnackbarFactory.showNewError(this, rootView(), getString(R.string.password_is_empty))
            return false
        } else if (email.isNotEmpty() && !isEmailValid(email)) {
            SnackbarFactory.showNewError(this, rootView(), getString(R.string.email_address_not_valid))
            return false
        }

        return true
    }

    private fun hideKeyboard() {
        val view = this.currentFocus
        view?.let { v ->
            val imm =
                this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(v.windowToken, 0)
        }
    }

    override fun showHomePage() {
        startHomeActivity()
        finish()
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left)
    }

    override fun setPresenter(presenter: Presenter) {
        this.presenter = presenter
    }

    override fun context(): Context = this

    override fun showUnexpectedError(error: Throwable) {
        val errorMessage = error.message
        if (errorMessage != null) {
            SnackbarFactory.showNewError(this, rootView(), errorMessage)
        }
    }

    override fun showLoading() {
        tvLogin.gone()
        pbLogin.show()
    }

    override fun hideLoading() {
        pbLogin.gone()
        tvLogin.show()
    }

    override fun rootView(): CoordinatorLayout = rootView

    override fun onBackPressed() {
        if (isTaskRoot) {
            showExitAlertDialog {
                finishAffinity()
            }
        } else {
            super.onBackPressed()
        }
    }
}