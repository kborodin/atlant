package com.example.atlanttest.service

import com.example.atlanttest.data.model.AuthToken
import com.example.atlanttest.data.model.UserInfo
import com.example.atlanttest.utils.apiUtils.AUTH
import com.example.atlanttest.utils.apiUtils.CURRENT
import com.example.atlanttest.utils.apiUtils.END
import com.example.atlanttest.utils.apiUtils.REFRESH
import com.example.atlanttest.utils.apiUtils.SESSIONS
import com.google.gson.JsonObject
import io.reactivex.Completable
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface ApiService {

    @POST(AUTH)
    fun tryToLogin(@Body params: JsonObject): Observable<AuthToken>

    @POST(SESSIONS + REFRESH)
    fun refreshToken(): Observable<AuthToken>

    @POST(SESSIONS + END)
    fun tryToLogout(@Body params: JsonObject): Completable

    @GET(CURRENT)
    fun getUserInfo(): Observable<UserInfo>
}